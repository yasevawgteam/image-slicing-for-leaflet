<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

define('BASE_DIR', str_replace("\\", "/", $_SERVER['DOCUMENT_ROOT'] . "/monitoring/tiler/"));

$src = BASE_DIR . "img/src/";

?>

<div class="desc">
	Нарезка тайлов для карты leaflet. <br>
	<strong style="color: red;">ВАЖНО!</strong> Формат картинки только .png!
</div>
<div class="desc">
	<form enctype="multipart/form-data" action="?upload=1" method="POST">
		<p>
			<label>Этаж 1<input type="radio" name="floar" value="m1" checked></label>
			<label>Этаж 2<input type="radio" name="floar" value="m2"></label>
		</p>
		<!-- Название элемента input определяет имя в массиве $_FILES -->
		Загрузка: <input name="file" type="file" />
		<input type="submit" value="Загрузить" />
	</form>
</div>

<?
if(isset($_REQUEST["upload"]) && $_REQUEST["upload"]) {
	
	$uploadfile = BASE_DIR .  "img/src/5.png";
	
	if (end(explode('.', $_FILES['file']['name'])) == strtolower("png")) {
		if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {	?>
			
			<div class='rezult'>
				Файл  успешно загружен.
				<a href="?start=1&floar=<?=$_POST['floar']?>" class="button">Начать нарезку изображений</a>
			</div>
	<?
		} else {
			echo "<div class='rezult error'>Ошибка загрузки файла!</div>";
		}		
	} else {
		echo "<div class='rezult error'>Неверный формат изображения!<br>Верный формат <b>png</b></div>";
	}
}


if(isset($_REQUEST["start"]) && $_REQUEST["start"]){
	if(slicing ($src, $_GET['floar'])) {
		echo "<div class='rezult'>Нарезка завершена. Нарезанные изображения доступны в папке 'tiler/img/'</div>";
	}
} else {
	exit;
}

function slicing ($src, $floarDir) {
	
	$error = [];	

	if (!loadImages($src . "5", $src . '5.png')) {
		$error[] = "<div class='rezult error'>Ошибка нарезки максимального изображения. Выгрузка остановлена.<div>";
	}
	
	if (!ResizeImage($src . '5.png', $src, '4.png')) {
		$error[] = "<div class='rezult error'>Ошибка сжатия изображения. Выгрузка остановлена.<div>";
	} else {
		if (!loadImages($src . "4", $src . '4.png')) {
			$error[] = "<div class='rezult error'>Ошибка нарезки 4-го изображения. Выгрузка остановлена.<div>";
		}		
	}
	
	if (!ResizeImage($src . '4.png', $src, '3.png')) {
		$error[] = "<div class='rezult error'>Ошибка сжатия изображения. Выгрузка остановлена.<div>";
	} else {
		if (!loadImages($src . "3", $src . '3.png')) {
			$error[] = "<div class='rezult error'>Ошибка нарезки 3-го изображения. Выгрузка остановлена.<div>";
		}		
	}		
	
	if (empty($error)) {
		sendNewImage ($src, $floarDir);
	};
	
	foreach ($error as $val) {
		echo $val . "<br>";
	}
}

function sendNewImage ($src, $floarDir) {
	$mapFolders = ["5", "4", "3"];
	$error = [];
	
	foreach ($mapFolders as $folder) {
		$files = dirToArray($src . $folder . "/");
		
		foreach ($files as $file) {			
			if (!uploadOverwrite ("touch_mall/floars/" . $floarDir . "/" . $folder . "/" . $file, $src . $folder . "/" . $file)) {
				$error[] = "<div class='rezult error'>При выгрузке файла " . $file . "в папку " . $folder . " произошла ошибка. Возможно файл не выгружен, но это не точно =)<div>";
			}
		}
	}
	
	if (empty($error)) return true;
	
	foreach ($error as $val) {
		echo $val . "<br>";
	}
}

function loadImages($folder, $src) {
	
	if(!file_exists($src)){
		echo "<div class='rezult error'>Исходное изображение отсуствует " . $src . "<br>" . "\n </div>";
		return false;
	}

	list($w_i, $h_i, $type) = getimagesize($src); // получаем ширину, высоту и расширение исходного изображения
	
	$w_s = 256; // ширина нарезанного изображения
	$h_s = 256;	// высота нарезанного изображения
	
	$v_d = ceil($w_i/$w_s); // количество циклов по ширине
	$h_d = ceil($h_i/$h_s); // количество циклов по высоте
	
	$w_i = $w_s * $v_d; // задаем новую ширину исходного изображения по ширине нарезанного изображения
	$h_i = $h_s * $h_d; // задаем новую высоту исходного изображения по высоте нарезанного изображения

	$allowedTypes = array( 
				1,  // [] gif 
				2,  // [] jpg 
				3  // [] png
		);

	if (!in_array($type, $allowedTypes)) {
		echo "<div class='rezult error'>Ошибка загрузки изображения " . $src . "<br>" . "\n </div>";
		return false; 
	}

	$folder = $folder . "/";
	
	$rezults_arr = array();
	switch ($type) { 
		case 1 : 
			$ext = ".gif";
			$img = imageCreateFromGif($src);
			for ($i=0; $i < $v_d; $i++) { 
				for ($j=0; $j < $h_d; $j++) {
					$name = $i . "-" . $j . $ext;
					$x = $j * $w_s;
					$y = $i * $h_s;
					$tumb = getTumbs($img, $w_s, $h_s, $x, $y);
					$rezults_arr[] = $name;
					imagegif($tumb, $name);
				}
			}

			return $rezults_arr;
		case 2 : 
			$ext = ".jpg";
			$img = imageCreateFromJpeg($src);
			for ($i=0; $i < $h_d; $i++) { 
				for ($j=0; $j < $v_d; $j++) {
					$name = $j . "-" . $i . $ext;
					$x = $i * $w_s;
					$y = $j * $h_s;
					$tumb = getTumbs($img, $w_s, $h_s, $x, $y);
					$rezults_arr[] = $name;
					imagejpeg($tumb, $name, 100);
				}
			}

			return $rezults_arr;

		case 3 :
			$ext = ".png";
			$img = imageCreateFromPng($src);
			for ($i=0; $i < $v_d; $i++) { 
				for ($j=0; $j < $h_d; $j++) {
					$name = $folder . $i . "-" . $j . $ext;
					$x = $i * $w_s;
					$y = $j * $h_s;
					$tumb = getTumbs($img, $w_s, $h_s, $x, $y);
					$rezults_arr[] = $name;
					imagepng($tumb, $name);
				}
			}

			return $rezults_arr;
	}
	return false;    
}

function getTumbs($img, $w, $h, $x, $y){
	$img_o = imagecreatetruecolor($w, $h);
	imagecopyresampled($img_o, $img, 0, 0, $x, $y, $w, $h, $w, $h);

	return $img_o;
}

function ResizeImage ($filename, $path_save, $new_filename)
{
	/*
	* Адрес директории для сохранения картинки
	*/
	$dir = $path_save;
	
	/*
	* Извлекаем формат изображения
	*/
	$ext  = strtolower(strrchr(basename($filename), "."));
	
	/*
	* Допустимые форматы
	*/
	$extentions = array('.jpg', '.gif', '.png', '.bmp');
	
	/*
	* Уменьшение на 50%
	*/
	$percent = 0.5;

	if (in_array($ext, $extentions)) {
	
		 list($width, $height) = getimagesize($filename); // Возвращает ширину и высоту
		 $newheight    = $height * $percent;
		 $newwidth    = $width * $percent;
	
		 $thumb = imagecreatetruecolor($newwidth, $newheight);
	
		 switch ($ext) {
			 case '.jpg':
				 $source = @imagecreatefromjpeg($filename);
				 break;
			
			  case '.gif':
				 $source = @imagecreatefromgif($filename);
				 break;
			
			  case '.png':
				 $source = @imagecreatefrompng($filename);
				 break;
			
			  case '.bmp':
				  $source = @imagecreatefromwbmp($filename);
		  }

		/*
		* Функция наложения, копирования изображения
		*/
		imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
	
		/*
		* Создаем изображение
		*/
		switch ($ext) {
			case '.jpg':
				imagejpeg($thumb, $dir . $new_filename, 100);
				break;
				
			case '.gif':
				imagegif($thumb, $dir . $new_filename);
				break;
				
			case '.png':
				imagepng($thumb, $dir . $new_filename);
				break;
				
			case '.bmp':
				imagewbmp($thumb, $dir . $new_filename);
				break;
		}    
    } else {
        return false;
    }

    /* 
    *  Очищаем оперативную память сервера от временных файлов
    */
	@imagedestroy($thumb);         
	@imagedestroy($source);  
		
	return true;
}

function uploadOverwrite ($newPath, $file = '') {
	$token = 'токен';
		
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
		CURLOPT_PORT => "443",
		CURLOPT_URL => 'https://cloud-api.yandex.net/v1/disk/resources/upload?path='.$newPath.'&overwrite=true',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"authorization: OAuth ".$token,
			"cache-control: no-cache"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	
	curl_close($curl);

	if ($err) {
		return false;
	} else {
		
		$responseData = json_decode($response, true);
		$fp = fopen($file, 'r');
		
		$ch = curl_init();
		curl_setopt_array($ch, array(
			CURLOPT_PORT => "443",
			CURLOPT_URL => $responseData['href'],
			CURLOPT_UPLOAD => true,
			CURLOPT_INFILESIZE => filesize($file),
			CURLOPT_INFILE => $fp,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $responseData['method'],
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/x-www-form-urlencoded"
			),
		));

		$response2 = curl_exec($ch);
		$err2 = curl_error($ch);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		//print_r($http_code);
		
		if ($err2) {
			return false;
		} else {
			return true;
		}		
	}		
}

function dirToArray($dir) { 
   
   $result = array();
   $cdir = scandir($dir);
   
   foreach ($cdir as $key => $value) { 
		if (!in_array($value, array(".",".."))) { 
			if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) { 
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			} else { 
				$result[] = $value; 
			} 
		} 
	}
   
   return $result; 
}
?>